const accountSid = process.env.ACCOUNT_TWILIO || 'ACa75dbb44acbd384cda058cb165b74043';
const authToken = process.env.TOKEN_TWILIO || 'eefe3e12df5d57b6397023ea25f60e80';
const client = require('twilio')(accountSid, authToken);

exports.sendMessage = (message, to) => {
  return client
    .messages
    .create({
      body: message,
      from: process.env.NUMBER_TWILIO || 551142000294,
      to
    });
};
