const TwilioService = require('./services/twilio-service');

console.log('testing SMS sending...');
TwilioService.sendMessage('Ola. essa é uma mensagem do Twilio', '+5585991799572').then(result => {
  console.log(result);
}).catch(error => {
  console.log(error);
});

module.exports = TwilioService;
